﻿namespace Graylog.Client.Enums
{
    public enum SeveritylevelEnum
    {
        Emergency,
        Alert,
        Critical,
        Error,
        Warning,
        Notice,
        Informational,
        Debug
    }
}

﻿using Graylog.Client.Enums;
using Graylog.Client.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Graylog.Client
{
    public class GraylogClient
    {
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Facility { get; set; }
        public bool ClientSideTimestamps { get; set; }

        public GraylogClient(string address, string facility = null, bool clientSideTimestamps = true, string username = null, string password = null)
        {
            Address = address;
            Username = username;
            Password = password;
            Facility = facility;
            ClientSideTimestamps = clientSideTimestamps;
        }

        public void Log(string shortMessage, string fullMessage = "", SeveritylevelEnum? severitylevel = null)
        {
            RequestModel requestModel = new RequestModel();
            if (severitylevel != null)
            {
                requestModel.Level = (int)severitylevel.Value;
            }
            requestModel.ShortMessage = shortMessage;
            requestModel.FullMessage = fullMessage;

            SendToGraylog(requestModel);
        }


        public void Log(string shortMessage, Exception exception)
        {
            RequestModel requestModel = new RequestModel();
            requestModel.Level = (int)SeveritylevelEnum.Error;
            requestModel.IsException = 1;
            requestModel.ShortMessage = shortMessage;
            requestModel.FullMessage = exception.ToString();

            SendToGraylog(requestModel);
        }

        public void Log(Exception exception)
        {
            RequestModel requestModel = new RequestModel();
            requestModel.Level = (int)SeveritylevelEnum.Error;
            requestModel.IsException = 1;
            requestModel.ShortMessage = exception.Message.ToString();
            requestModel.FullMessage = exception.ToString();

            SendToGraylog(requestModel);
        }

        private void SendToGraylog(RequestModel requestModel)
        {
            HttpClient httpClient = new HttpClient();
            requestModel.Facility = Facility;
            requestModel.Version = "1.1";
            requestModel.Host = System.Environment.MachineName;

            // If a basic auth user and pass is supplied add it into the request headers. Useful for reverse proxy for auth.
            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                    System.Text.Encoding.ASCII.GetBytes(
                        string.Format("{0}:{1}", Username, Password))));
            }

            // If they want client timestamps then inject it.
            if (ClientSideTimestamps == true)
            {
                requestModel.Timestamp = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            }

            httpClient.PostAsJsonAsync(Address, requestModel);
        }
    }
}

﻿using Newtonsoft.Json;

namespace Graylog.Client.Models
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    class RequestModel
    {
        // Specification normal fields.
        [JsonProperty(PropertyName = "version")]
        public string Version { get; set; }
        [JsonProperty(PropertyName = "host")]
        public string Host { get; set; }
        [JsonProperty(PropertyName = "short_message")]
        public string ShortMessage { get; set; }
        [JsonProperty(PropertyName = "full_message")]
        public string FullMessage { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public double Timestamp { get; set; }
        [JsonProperty(PropertyName = "level")]
        public int? Level { get; set; }

        // Specification additional fields.
        [JsonProperty(PropertyName = "_facility")]
        public string Facility { get; set; }
        [JsonProperty(PropertyName = "_line")]
        public int? Line { get; set; }

        // Custom additional fields.
        [JsonProperty(PropertyName = "_error_code")]
        public string ErrorCode { get; set; }
        [JsonProperty(PropertyName = "_is_exception")]
        public int? IsException { get; set; }
    }
}
